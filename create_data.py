import csv

from common.generate_data import *
from common.read_config import get_data_type
from tqdm import tqdm
data_types = get_data_type()
root_path = os.path.abspath(os.path.dirname(__file__))
if not os.path.exists(root_path + "/data_files/"):
    os.makedirs(root_path + "/data_files/")
csv_file = root_path + "/data_files/" + data_types['file_name'] + ".csv"


def create_data():
    # 获取要生成数据配置清单

    # 生成表头
    headers = []
    all_types = ['numerical', 'temporal', 'textual_zh', 'textual_en']
    for test_type in all_types:
        if test_type in data_types.keys():
            col_num = int(data_types[test_type]['insert_column'])
            for header_number in range(col_num):
                headers.append(test_type + "-" + str(header_number + 1))


    # 生成数据
    tax_documents = int(data_types['tax_documents'])
    rows_length = int(data_types['documents'])
    rows = []
    for row in tqdm(range(rows_length)):
        data_lines = []
        for header in headers:
            data_type = header.split("-")[0]
            if data_type == 'numerical':
                data = generate_numerical()
            if data_type == 'temporal':
                data = generate_temporal()
            if data_type == 'textual_zh':
                if row < tax_documents:
                    data = generate_textual_zh(True)
                else:
                    data = generate_textual_zh(False)
            if data_type == 'textual_en':
                data = generate_textual_en()
            data_lines.append(data)
        rows.append(tuple(data_lines))
    print("文件名:", csv_file)
    print("行数:", rows_length)
    print("列:", headers)
    # 将数据写入csv文件中

    with open(csv_file, "w", encoding="utf-8") as f:
        write_data = csv.writer(f)
        write_data.writerow(headers)
        write_data.writerows(rows)


if __name__ == '__main__':
    create_data()
