import itertools
import os
import random
import re
import time

root_path = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
zh_words_list = root_path + "/config/words_list_zh.txt"
en_words_list = root_path + "/config/words_list_en.txt"
tax_file = root_path + "/config/create_data_taxonomy.json"


# 随机生成固定长度的数字
def generate_int(num):
    first = random.sample(range(1, 10), 1)
    rest = random.sample(range(0, 10), int(num) - 1)
    data = first + rest
    data = [str(x) for x in data]
    data = "".join(itertools.chain(*data))
    return data

    # random.randint(1,10000) 随机生成1~10000之间的数字


def generate_float(num):
    first = random.sample(range(1, 10), 1)
    rest = random.sample(range(0, 10), int(num) - 2)
    random_float = random.sample(range(0, num - 2), 1)[0]
    rest.insert(random_float, ".")
    data = first + rest
    data = [str(x) for x in data]
    data = "".join(itertools.chain(*data))
    return data


def generate_int_float(num):
    int_data = generate_int(num)
    float_data = generate_float(num)
    return random.choice((int_data, float_data))


def generate_time(time_start, time_end):
    startArray = time.strptime(time_start, "%Y-%m-%d")
    start = int(time.mktime(startArray))
    endArray = time.strptime(time_end, "%Y-%m-%d")
    end = int(time.mktime(endArray))
    t = random.randint(start, end)
    date_random = time.localtime(t)
    date = time.strftime("%Y-%m-%d", date_random)
    return date


def read_zh_txt(file_path):
    with open(file_path, 'r', encoding='utf-8') as f:
        d = f.read()
        d = d.replace("\n", "")
        d = d.replace(" ", "")
        return d


def read_en_txt(file_path):
    with open(file_path, 'r', encoding='utf-8') as f:
        d = f.read()
        d = d.split("\t")
        d.remove("")
        return d


def generate_sentences_zh(num, is_tax):
    all_word = read_zh_txt(zh_words_list)
    random_text = all_word.split("\t")
    # 在词汇中随机选择300个词汇
    tax_list = read_taxonomy_json()
    tax_list = random.sample(tax_list, 1)
    zh_word_list = random.sample(random_text, 300)
    if is_tax:
        zh_word_list = list(set(tax_list + zh_word_list))
    else:
        while tax_list[0] in zh_word_list:
            zh_word_list.remove(tax_list[0])

    zh_sentences = "".join(itertools.chain(*zh_word_list))
    # 在300个词汇选择 num 个字做为名子
    sentence = zh_sentences[0:num]
    return sentence


def generate_sentences_en(num):
    all_word = read_en_txt(en_words_list)
    en_word_list = random.sample(all_word, num)
    en_sentences = " ".join(en_word_list)
    return en_sentences


def read_taxonomy_json():
    with open(tax_file, 'r', encoding='utf-8') as f:
        tax = f.read()
    tax_words = re.findall('\{\"sentence\":\[(.+?)\]\}', tax)
    while ']}' in tax_words:
        tax_words.remove("]}")
    tax_list = []
    for tax in tax_words:
        tax_list = tax_list + tax.replace("\"", "").split(",")
    return tax_list


if __name__ == '__main__':
    print("^^^^^^^^^^^^^^^")
    d = generate_sentences_en(10)
    print("----------------")

    print(d)
