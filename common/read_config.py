import configparser
import os

cf = configparser.ConfigParser()
root_path = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
config_path = root_path + "/config/data_config.ini"
cf.read(config_path)


def get_data_type():
    data_sections = cf.sections()
    data_type = {}
    for data_section in data_sections:
        if data_section == 'data_stream':
            data_type.update({"documents": cf.get(data_section, "documents")})
            data_type.update({"file_name": cf.get(data_section, "file_name")})
            data_type.update({"tax_documents": cf.get(data_section, "tax_documents")})
        else:
            if cf.getboolean(data_section, "add"):
                data_options = cf.options(data_section)
                data_options.remove('add')
                option_dict = {}
                for data_option in data_options:
                    option_dict.update({data_option: cf.get(data_section, data_option)})
                data_type.update({data_section: option_dict})
    return data_type


if __name__ == '__main__':
    a = get_data_type()
    print(a)
