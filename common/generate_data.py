from common.data_api import *
from common.read_config import get_data_type

data_types = get_data_type()


def generate_numerical():
    number_length = data_types['numerical']['number_length']
    number_type = data_types['numerical']['number_type']
    if number_type == '1':
        data = generate_int(number_length)
    if number_type == '2':
        data = generate_float(number_length)
    if number_type == '3':
        data = generate_int_float(number_length)
    return data


def generate_temporal():
    time_start = data_types['temporal']['time_from']
    time_end = data_types['temporal']['time_to']
    data = generate_time(time_start, time_end)
    return data


def generate_textual_zh(is_tax):
    text_length = data_types['textual_zh']['text_length']
    data = generate_sentences_zh(int(text_length), is_tax)
    return data


def generate_textual_en():
    text_length = data_types['textual_en']['text_length']
    data = generate_sentences_en(int(text_length))
    return data


if __name__ == '__main__':
    test = generate_textual_en()
    print(test)
