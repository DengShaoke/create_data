dockerid=`docker image build -t smoke-test:$1 -f python.dockerfile . | grep built | awk '{print $3}'`
docker tag $dockerid registry.gitlab.com/stratifyd/frontend-dev/frontend-test-prototype/create_data:$1
docker push registry.gitlab.com/stratifyd/frontend-dev/frontend-test-prototype/create_data:$1
